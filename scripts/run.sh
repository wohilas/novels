#!/usr/bin/env sh

novel=${1:-overlord-ln}

set -e

# COLS="book\talias\tvolume\tchapter\tverse\ttext"
novelPath="$(find . -name "$novel")"

[ -z "$novelPath" ] && echo "$novel not found" && exit 1

readarray -t chapters < <(find "$novelPath" -type f -name "*.html")

index=1
len=${#chapters[@]}

for ch in ${chapters[@]}; do
    outFile=$(echo "$ch" | sed 's/html/tsv/')
    # if tsv file already exists => skip
    [ -z "$(find "$outFile" 2>/dev/null)" ] || continue
    printf "\rparsing $index out of $len"
    # check if outFile exists
    ./parse_html.sh "$ch" "$outFile"
    index=$((index + 1))
done

endFile="$novelPath/${novel}.tsv"
# to avoid - cat: input file is output file
[ -z "$(find "$endFile" 2>/dev/null)" ] || rm "$endFile"
# cat wont work with "" (No such file or directory); no idea why tho
cat ${novelPath}/*.tsv > "$endFile"
# sort by volumes then chapters and then by verses
sort -t$'\t' -nk3 -nk4 -nk5 -o "$endFile" "$endFile"
# sed -i "1 s/^/${COLS}\n/" "$endFile"

mkdir -p data
# copy/merge output
mv "$endFile" "./data/${novel}.tsv"
# clear source data
./clear_data.sh "$novel"
