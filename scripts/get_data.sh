#!/usr/bin/env sh

set -e

novel=${1:-overlord-ln}

startURL="https://readnovelfull.com/${novel}.html"
ruleURL="https://readnovelfull.com/${novel}/*"

# extract links
wget -r -nv -nc -l 0 -w .1 -o wget.log --accept-regex "$ruleURL" "$startURL"
