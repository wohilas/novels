#!/usr/bin/env sh

set -e

NOVEL=${1}

NOVELPATH=$(find . -name "$NOVEL" -type d)
[ -z "$NOVELPATH" ] || rm -rf "$NOVELPATH"

FRONTPAGE=$(find . -name "${NOVEL}.html")
[ -z "$FRONTPAGE" ] || rm "$FRONTPAGE"
