#!/usr/bin/env sh

FILEPATH=${1:-chapter-11-volume-2.html}
OUTFILE=${2:-}

[ -z "$OUTFILE" ] && OUTFILE=$(echo "$FILEPATH" | sed 's/html/tsv/')

title=$(xmllint --html --xpath '//a[@class="novel-title"]/text()' "$FILEPATH" 2>/dev/null)
# first 3 letters
shortName=$(echo "$title" | sed -n "s/\([a-zA-Z]\{3\}\)\(.*\)/\1/p")

chapterTitle=$(xmllint --html --xpath '//span[@class="chr-text"]/text()' "$FILEPATH" 2>/dev/null)

# chapters and volumes are hard -_-:
# Chapter 5
# Chapter 81 - Volume 11
# Volume 14: Prologue (Part 2)
# Volume 14: Chapter 2 - Countdown to Extinction (Part 5)
chapterNum=$(echo "$chapterTitle" | sed -n 's/.*Chapter \([0-9]\+\).*/\1/p')
volNum=$(echo "$chapterTitle" | sed -n 's/.*Volume \([0-9]\+\).*/\1/p')
[ -z "$volNum" ] && volNum=1
# it should be prologue; so maybe 0 index is okay
[ -z "$chapterNum" ] && chapterNum=0

# vCount=$(xmllint --html --xpath 'count(//p)' "$FILEPATH" 2>/dev/null)

# SC2039: In POSIX sh, arrays are undefined. -_-
# https://github.com/EricChiang/pup
# readarray -t verses < <(pup -f "$FILEPATH" 'p text{}')
IFS=$'\n' verses=($(pup -f "$FILEPATH" 'p text{}'))

len=${#verses[@]}
i=1
ti=$i

while [ $i -lt "$len" ]; do
    verse=${verses[$i]}
    i=$((i+1))

    # skip unwanted verses
    (echo "$verse" | grep -vq '[[:alnum:]]') && continue

    # write verse to tsv
    echo -e "$title\t$shortName\t$volNum\t$chapterNum\t$ti\t$verse" >> "$OUTFILE"
    # tsv verse index
    ti=$((ti+1))
done

