package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/gocarina/gocsv"
	"github.com/gocolly/colly/v2"
)

var (
	OVERLORD_URL = "https://readnovelfull.com/overlord-ln/chapter-1.html"
	currentURL   = OVERLORD_URL
	commaType    = '\t'
	outName      = "data/overlord.tsv"
	chNumRe      = regexp.MustCompile(`(\d+)`)
	wordRe       = regexp.MustCompile(`(\w+)`)
)

type item struct {
	SourceURL     string `tsv:"sourceURL"`
	Title         string `tsv:"title"` // novel
	Volume        string `tsv:"volume"`
	ChapterNumber int    `tsv:"chapterNum"`
	Chapter       string `tsv:"chapter"`
	VerseNumber   int    `tsv:"verseNum"`
	Verse         string `tsv:"verse"` // paragraph
}

func main() {

	novel := []*item{}
	c := colly.NewCollector(
		colly.AllowedDomains("readnovelfull.com"),
		colly.URLFilters(
			regexp.MustCompile("https://readnovelfull.com/overlord-ln/*"),
		),
	)

	// Find and visit all links
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		e.Request.Visit(e.Attr("href"))
	})

	c.OnHTML("div[id=chapter]", func(e *colly.HTMLElement) {
		title := e.ChildText("a[class=novel-title]")
		verses := e.ChildTexts("p")
		chapterVolume := e.ChildText("span[class=chr-text]")
		chvSlice := strings.Split(chapterVolume, " - ")
		chapterName := chvSlice[0]
		chapterNum := chNumRe.Find([]byte(chapterName))
		volume := "Volume 1"
		if len(chvSlice) > 1 {
			volume = chvSlice[1]
		}

		for vi, verse := range verses {

			// filter junk
			if !wordRe.MatchString(verse) {
				continue
			}

			chapter := &item{}
			chapter.Title = title
			chapter.Verse = verse
			chapter.SourceURL = currentURL
			chapter.Volume = volume
			chapter.Chapter = chapterName
			chapter.ChapterNumber, _ = strconv.Atoi(string(chapterNum))
			chapter.VerseNumber = vi
			novel = append(novel, chapter)
		}
	})

	c.OnRequest(func(r *colly.Request) {
		currentURL = r.URL.String()
		fmt.Println("Visiting", r.URL)
	})

	c.Visit(OVERLORD_URL)

	fmt.Println(len(novel))

	writeToCSV(outName, novel)
}

func writeToCSV(filename string, data []*item) error {

	gocsv.SetCSVWriter(func(out io.Writer) *gocsv.SafeCSVWriter {
		writer := csv.NewWriter(out)
		writer.Comma = commaType
		return gocsv.NewSafeCSVWriter(writer)
	})

	lf, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		return err
	}
	defer lf.Close()
	err = gocsv.MarshalFile(data, lf)
	if err != nil {
		return err
	}

	return nil
}
