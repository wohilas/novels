PREFIX = /usr/local

novels: scripts/novels.sh novels.awk novels.tsv
	cat scripts/novels.sh > $@
	echo 'exit 0' >> $@
	echo '#EOF' >> $@
	tar cz novels.awk novels.tsv >> $@
	chmod +x $@

test: scripts/novels.sh
	shellcheck -s sh ./scripts/*

clean:
	rm -f novels

install: novels
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f novels $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/novels

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/novels

.PHONY: test clean install uninstall
