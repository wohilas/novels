module novels

go 1.15

require (
	github.com/gocarina/gocsv v0.0.0-20201208093247-67c824bc04d4
	github.com/gocolly/colly/v2 v2.1.0
)
